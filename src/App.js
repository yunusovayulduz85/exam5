import Router from './router/Router';
import BasketContextProvider from './context/BasketContext';
function App() {
  return (
    <BasketContextProvider>
      <Router />
    </BasketContextProvider>
  )
}
export default App
//*routerlarni ,rootLayout ni componenta ,page larni yozishing kerak