import React, { useContext } from 'react';
import "../App.css"
import { BContext } from '../context/BasketContext';
import "../App.css"
import { toast } from 'react-toastify';
function CategoryCart({item}) {
    const {basket,setBasket,like,setLike}=useContext(BContext);
    const addBasket = (product) => {
        if (basket.indexOf(product) !== -1) return;
      setBasket([...basket, product])
    }
    const addLike=(likeProduct)=>{
      if(like.indexOf(likeProduct)!==-1) return
      setLike([...like,likeProduct])
    }
  return (
      <div className='col mt-5 pt-2'>
          <div className='row mx-1 p-2'>
              <div className='bg-body-tertiary rounded py-2 shadow  bg-body-tertiary rounded d-flex align-items-center justify-content-center'>
                  <div className='' height={"300px"}>
                      <img src={item.thumbnail} alt={item.title} height={"300px"} width={"300px"} className='images' />
                  </div>
                  <div className='mx-3'>
                    <p className='fw-bold'>{item.title}</p>
                      <p className=' brand fw-bold text-danger'>
                          ${item.price}
                      </p>
                  <div className='d-flex align-items-center justify-content-center '>
                    <button className='productBtn px-3 btn btn-warning text-white' onClick={()=>addBasket(item)}>Add to Cart</button>
              <button className='btn btn-warning text-white mx-1' onClick={()=>addLike(item)}> <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
              </svg></button>
                  </div>
                  </div>
                     
                  
              </div>
          </div>
      </div>
  )
}

export default CategoryCart