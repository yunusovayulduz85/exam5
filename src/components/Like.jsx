import React, { useContext, useState } from 'react'
import "../App.css"
import { BContext } from '../context/BasketContext';
let totalPrice = 0;
function Basket({ item, removeLikedProduct }) {
    const { doubleProduct } = useContext(BContext)
    sessionStorage.setItem("basketProduct", item);
    const [counter, setCounter] = useState(1);
    return (
        <div className='d-flex align-items-center justify-content-center'>
            <div className='pt-5 w-50'>
                <div className='d-flex bg-body-tertiary px-5 align-items-center justify-content-between rounded rounded-2  shadow p-5 ps-0 bg-body-tertiary rounded'>
                    <div className='pt-5'></div >
                    <div className='' height={"300px"}>
                        <img src={item.thumbnail} width={"300px"} height={"300pc"} className='images' />
                    </div>
                    <div>
                        <p className='fw-bold text-primary-emphasis fs-5'>{item.title}</p>
                        <p className='fw-bold text-danger'>${item.price}</p>
                        <button className='btn btn-secondary' onClick={() => removeLikedProduct(item.id)}>Remove</button>

                    </div>
                </div >
            </div >

        </div>
       
    )
}

export default Basket