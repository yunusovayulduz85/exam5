import React, { useContext } from 'react'
import { useParams } from 'react-router-dom'
import useAxios from "../hooks/useAxios";
import "../App.css"
import { BContext } from '../context/BasketContext';
function ProductsDetail() {
  const {basket,setBasket,like,setLike}=useContext(BContext);
  const addBasket = (product) => {
    if (basket.indexOf(product) !== -1) return;
    setBasket([...basket, product])
  }
  const addLike = (likeProduct) => {
    if (like.indexOf(likeProduct) !== -1) return
    setLike([...like, likeProduct])
  }
  const { id } = useParams();
  const { res,loader } = useAxios("/products");
  return <div>
    {
      res.map((item, index) => {
        if (item.id == id) {
          return loader ? (<div className="spinner-border text-primary" role="status" >
            <span className="visually-hidden">Loading...</span>
          </div>):( <div key={index} className='d-flex align-items-center justify-content-center pt-5'>
            {<div className='d-flex bg-body-tertiary px-5 align-items-center justify-content-center rounded rounded-2 my-3 shadow p-3 mb-5 bg-body-tertiary rounded w-50 mt-5'>
              <div className='mx-5' height={"300px"}>
                <img src={item.thumbnail} className='images' width={"300px"} height={"300px"}/>
              </div>
              <div className='mx-5'>
                  <p className='fw-bold'>{item.title}</p>
                   <p className='fw-bold text-danger'>
                    ${item.price}
                  </p>                 
                  <button className='productBtn px-3 btn btn-warning text-white' onClick={()=>addBasket(item)}>Add to Cart</button>
                  <button className='btn btn-warning mx-1 text-white' onClick={()=>addLike(item)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                      <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                    </svg>
                  </button>
              </div>
            </div>
            }
          </div>)
        }
      })
    }
  </div>

}

export default ProductsDetail