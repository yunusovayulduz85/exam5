import React, { useContext, useState } from 'react'
import "../App.css"
import { Link } from 'react-router-dom'
import { BContext } from '../context/BasketContext'
function ProductsList({ item }) {
  const { basket, setBasket, setDoubleProduct, like, setLike } = useContext(BContext);
  const addBasket = (product) => {
    if (basket.indexOf(product) !== -1) return;
    setBasket([...basket, product])
  }
  const addLike = (likeProduct) => {
    if (like.indexOf(likeProduct) !== -1) return
    setLike([...like, likeProduct])
  }
  return (
      <div className='col'>
        <div className='row mx-1 mb-1 p-2' >
          <div className='bg-body-tertiary rounded-3 py-3 shadow p-3 mb-5 bg-body-tertiary '>
            <div className='' height={"250px"}>
              <img src={item.thumbnail} alt={item.title} height={"250px"} width={"250px"} className='images rounded rounded-1' />
            </div>
            <div className='d-flex px-5z justify-content-between flex-column'>
              <Link to={"products/" + `${item.id}`} className='text-decoration-none border-bottom pb-2'><span className='brand_title fw-normal text-dark'>Brand:</span> <span className='fw-bold brand'>{item.brand.substring(0, 8)}</span> </Link> <br />
              <Link to={"products/" + `${item.id}`} className='fw-bold text-decoration-none product_title'>{item.title.substring(0, 8)}</Link>
              <Link to={"products/" + `${item.id}`} className='fw-medium text-danger text-decoration-none mb-4 d-flex align-items-center justify-content-center'>
                <p className=' brand fw-bold'>
                  ${item.price}
                </p>
              </Link>
            </div>
            <div className='d-flex justify-content-center align-items-center'>
              <button className=' fw-bold d-flex btn btn-warning text-white' onClick={() => addBasket(item)} >Add to Cart</button>
              <button onClick={() => addLike(item)} className='btn btn-warning text-white mx-1'> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
              </svg></button>
            </div>
          </div>
        </div>
      </div>


  )
}

export default ProductsList;