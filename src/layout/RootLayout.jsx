import React, { useContext } from 'react'
import "../App.css"
import { Link, Outlet } from 'react-router-dom'
import Shopping from "../assets/cart3.svg";
import { BContext } from '../context/BasketContext';
function RootLayout() {
  const { basket,like } = useContext(BContext);
  return (
    <div>
      <nav className="navbar navbar-expand-lg w-100 nav mt-0 position-fixed bg-warning mb-5">
        <div className="container-fluid" >
          <Link to={"/"} className="navbar-brand  fw-bold" >Products</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <ul className="navbar-nav" >
                <li className="nav-item">
                  <Link className="nav-link fw-medium " aria-current="page" to={"/fragrances"}>Fragrances</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium " to={"/groceries"}>Groceries</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium " to={"/homeDecoration"}>HomeDecoration</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium " to={"/laptops"}>Laptops</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium " to={"/skincare"}>Skincare</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium " to={"/smartphones"}>Smartphones</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className='d-flex align-items-center'>
            <div className='bg-white p-1 rounded like-wrapper mx-3'>
              <Link className='d-flex align-items-center justify-content-center text-decoration-none ' to={"/like"}>
                <div className='d-flex align-items-center justify-content-center '>
                  <svg   xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                  </svg>
                  <div className="nav px-1 rounded-circle d-flex align-items-center justify-content-center mb-3" >
                    <p className='text-decoration-none  fw-medium  m-0 '>{like.length}</p>
                  </div>
                </div>
              </Link>
            </div>
            <div className='bg-white p-1 rounded'>
              <Link className='d-flex align-items-center justify-content-center text-decoration-none ' to={"/basket"}>
                <div className='d-flex align-items-center justify-content-center '>
                  <img src={Shopping} width={"25px"} />
                  <div className="nav px-1 rounded-circle d-flex align-items-center justify-content-center mb-3" >
                    <p className='text-decoration-none  fw-medium  m-0 '>{basket.length}</p>
                  </div>
                </div>
              </Link>
          </div>
             
            </div>
        </div>
      </nav>
      <Outlet />
    </div>
  )
}

export default RootLayout