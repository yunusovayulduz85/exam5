import React from 'react'
import ProductsList from "../components/ProductsList";
import useAxios from "../hooks/useAxios";
import "../App.css"
function Home() {
  const { res, loader } = useAxios("/products");
  return (
    <div className='container text-center'>
      {/* <div id="carouselExampleControlsNoTouching" className="carousel slide mt-4 d-flex justify-content-center align-items-center" data-bs-touch="false">
        <div className="carousel-inner  bg-warning-subtle">
          <div className="carousel-item active d-flex justify-content-center py-5">
            <img src="https://i.dummyjson.com/data/products/1/1.jpg" className="d-block w-25 mx-5" alt="..."/>
            <img src="https://i.dummyjson.com/data/products/1/2.jpg" className="d-block w-25 mx-5" alt="..."/>
          </div>
          <div className="carousel-item d-flex justify-content-center py-5">
            <img src="https://i.dummyjson.com/data/products/2/1.jpg" className="d-block w-25 mx-5" alt="..."/>
            <img src="https://i.dummyjson.com/data/products/2/2.jpg" className="d-block w-25 mx-5" alt="..."/>
          </div>
          <div className="carousel-item d-flex justify-content-center py-5">
            <img src="https://i.dummyjson.com/data/products/5/1.jpg" className="d-block w-25 mx-5" alt="..."/>
            <img src="https://i.dummyjson.com/data/products/5/2.jpg" className="d-block w-25 mx-5" alt="..."/>
          </div>
          <div className="carousel-item d-flex justify-content-center py-5">
            <img src="https://i.dummyjson.com/data/products/6/4.jpg" className="d-block w-25 mx-5" alt="..."/>
            <img src="https://i.dummyjson.com/data/products/6/2.jpg" className="d-block w-25 mx-5" alt="..."/>
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <div className=' d-flex justify-content-center align-items-center mb-3'>
        <div className=' mt-4 w-100 bg-body-tertiary d-flex align-items-center px-2 py-2 rounded shadow-lg'>
          <div className='leftLine'>|</div>
        <p className='fw-bold text-secondary mx-3 fs-4'>SEE OUR PRODUCTS</p></div>
      </div> */}
     
      <div className='row'>
        {
          loader ? (<div className="spinner-border text-primary" role="status" >
            <span className="visually-hidden">Loading...</span>
          </div>) : (res?.map((item, i) =>          
              <ProductsList item={item} key={i} />
          ))
        }
      </div>
    </div>
  )
}

export default Home