import React, { useContext } from 'react'
import { BContext } from '../context/BasketContext'
import Like from "../components/Like";
const LikeList = () => {
    const {like,setLike}=useContext(BContext);
    const removeLikedProduct = (id) => setLike(like.filter((_) => _.id !== id))

  return (
    <div>
       {
        like?.map((item,i)=><Like item={item} key={i} removeLikedProduct={removeLikedProduct}/>)
       }
    </div>
  )
}

export default LikeList